// const express = require("express");
// require("dotenv").config();
// const cors = require("cors");
// const { MongoClient } = require("mongodb");
// const admin = require("firebase-admin");
// const serviceAccount = require("./dishco-c3698-firebase-adminsdk-wqkcl-a41e65fe00.json");

// // process.env.GOOGLE_APPLICATION_CREDENTIALS;

// // connecting link
// const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.cg9bhlx.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;

// const port = process.env.PORT || 8000;

// const app = express();
// app.use(cors());
// app.use(express.urlencoded({ extended: true }));
// app.use(express.json());

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount),
//   projectId: "potion-for-creators",
// });

// const client = new MongoClient(uri, {
//   useNewUrlParser: true,
//   useUnifiedTopology: true,
// });

// client.connect((err) => {
//   const orderCollection = client
//     .db("demoDatabase")
//     .collection("demoCollection");

//   //data adding
//   // app.post("/addFood", async (req, res) => {
//   //   const food = req.body;
//   //   const info = await orderCollection.insertOne(food).then((result) => {
//   //     res.send(result);
//   //   });
//   // });
//   app.post("/send", function (req, res) {
//     const receivedToken = ["my device token"];

//     const message = {
//       notification: {
//         title: "Notif",
//         body: "This is a Test Notification",
//       },
//       token: receivedToken,
//     };

//     admin
//       .messaging()
//       .send(message)
//       .then((response) => {
//         res.status(200).json({
//           message: "Successfully sent message",
//           token: receivedToken,
//         });
//         console.log("Successfully sent message:", response);
//       })
//       .catch((error) => {
//         res.status(400);
//         res.send(error);
//         console.log("Error sending message:", error);
//       });
//   });

//   app.get("/getOrders", (req, res) => {
//     orderCollection?.find({}).toArray((err, documents) => {
//       if (err) {
//         console.error("Error retrieving orders:", err);
//         res.status(500).send("Error retrieving orders");
//       } else {
//         res.send(documents);
//       }
//     });
//   });

//   //end of connection for collection
// });

// // This is for testing hello world
// app.get("/", (req, res) => {
//   res.send("Hello World!");
// });

// // app.listen(port);
// app.listen(port, () => console.log(`connected database server${port}`));

const express = require("express");
require("dotenv").config();
const cors = require("cors");
const { MongoClient } = require("mongodb");
const admin = require("firebase-admin");
const serviceAccount = require("./dishco-c3698-firebase-adminsdk-wqkcl-a41e65fe00.json");

// process.env.GOOGLE_APPLICATION_CREDENTIALS;

// connecting link
const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.cg9bhlx.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`;

const port = process.env.PORT || 8000;

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  projectId: "dishco-c3698", // Your Firebase project ID
});

const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

client.connect((err) => {
  const orderCollection = client
    .db("demoDatabase")
    .collection("demoCollection");

  //data adding
  // app.post("/addFood", async (req, res) => {
  //   const food = req.body;
  //   const info = await orderCollection.insertOne(food).then((result) => {
  //     res.send(result);
  //   });
  // });
  app.post("/send", async function (req, res) {
    const receivedToken =
      "capbGsSkZFU1oAtd_6Aigz:APA91bHpVuKHyzr5zrn7tlorw9B32h1cFp5UQxftTRcRy4aFJbm0QHpMBsSxihlzxv3diuExGTJoYjfKQMUxmVGdmi_gwenTJv1vGHFE1xHNisz1glO3gBkH4oO9s1_HjFrbJ8VSUYhf";

    const message = {
      notification: {
        title: "good morning",
        body: "this is jahid from the usa",
      },
      token: receivedToken,
    };

    try {
      const response = await admin.messaging().send(message);

      res.status(200).json({
        message: "Successfully sent message",
        token: receivedToken,
      });

      console.log("Successfully sent message:", response);
    } catch (error) {
      res.status(400);
      res.send(error);
      console.log("Error sending message:", error);
    }
  });

  app.get("/getOrders", (req, res) => {
    orderCollection?.find({}).toArray((err, documents) => {
      if (err) {
        console.error("Error retrieving orders:", err);
        res.status(500).send("Error retrieving orders");
      } else {
        res.send(documents);
      }
    });
  });

  //end of connection for collection
});

// This is for testing hello world
app.get("/", (req, res) => {
  res.send("Hello World!");
});

// app.listen(port);
app.listen(port, () => console.log(`connected database server${port}`));
